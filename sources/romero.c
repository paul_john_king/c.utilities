/**	@file
 *	
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include "romero.h"

/** Try to write to an output stream a short help message about the program.
 *	
 *	@param stream -- An output stream.
 *	
 *	@param name -- A name.
 *	
 *	@return Try to write to `stream` a short help message about the program
 *	using `name` to identify the program, and return the number of characters
 *	written (excluding the terminating null character).  If an error occurs,
 *	return -1.
 */

static int write_short_help_message(FILE*const stream, const wchar_t*const name)
{

	int count ;

	if (stream == NULL || name == NULL)
	{
		return -1 ;
	}

	count = fwprintf(
		stream, 
		LR"(The command call

    %1$ls [-h|-H]

creates 10 zombie process and sleeps for 5 minutes.

*  `-h` -- Write a short help message to standard output and exit.

*  `-H` -- Write a long help message to standard output and exit.
)",
			name
		) ;

		if (count < 0)
		{
			return -1 ;
		}

		return count ;

	}

/** Try to write to an output stream a long help message about the program.
 *	
 *	@param stream -- An output stream.
 *	
 *	@param name -- A name.
 *	
 *	@return Try to write to `stream` a long help message about the program
 *	using `name` to identify the program, and return the number of characters
 *	written (excluding the terminating null character).  If an error occurs,
 *	return -1.
 */

static int write_long_help_message(FILE*const stream, const wchar_t*const name)
{

	int count ;
	int cumulative_count ;

	if (stream == NULL || name == NULL)
	{
		return -1 ;
	}

	cumulative_count = 0 ;

	count = fwprintf(
		stream, 
		LR"(Create zombie processes.

Usage
-----

)"
	) ;
	if (count < 0)
	{
		return -1 ;
	}
	else
	{
		cumulative_count += count ;
	}

	count = write_short_help_message(stream, name) ;
	if (count < 0)
	{
		return -1 ;
	}
	else
	{
		cumulative_count += count ;
	}



	count = fwprintf(
		stream, 
		LR"(
Details
-------

`%1$ls` creates 10 child processes and sleeps for 5 minutes.  Each child
process exits immediately after creation, and sends a `SIGCHILD` signal to the
parent process. However, the parent process does not invoke `wait()` in
response to the signal, and the exiting child remains in the process table as a
zombie process. If the parent process wakes and exits or is killed then each
child process is orphaned and `init` adopts each orphan process. `init` invokes
`wait()` at regular intervals, after which each orphan process dies.
)",
		name
	) ;

	if (count < 0)
	{
		return -1 ;
	}
	else
	{
		cumulative_count += count ;
	}

	return cumulative_count ;

}

/** @private */

int main(const int argc, const char*const*const argv)
{

	int status ;
	char ch ;

	setlocale(LC_ALL, "") ;

	//	If my command-line arguments contain a help option "-h" or "-H", I try
	//	to write to standard output a help message about myself and return
	//	success.  If an error occurs, I return failure.

	status = handle_help_options(argc, argv, &write_short_help_message, &write_long_help_message) ;
	if (status < 0)
	{
		return EXIT_FAILURE ;
	}
	if (status > 0)
	{
		return EXIT_SUCCESS ;
	}

	//	I create a curses interface for the user to control the program.

	//	TODO: At present, the options are not operational -- they merely report
	//	what they should do.

	initscr() ;
	noecho() ;
	cbreak() ;
	curs_set(0) ;

	//	TODO: Move this into a handler for SIGWINCH.

	int h, w ;
	getmaxyx(stdscr, h, w) ;

	if (h < 0)
	{
		return 123 ;
	}

	//	XXX: There is a lot of code here, but it is unique to the program, and
	//	I can't see how to encapsulate this within an external function.

	WINDOW* text = newwin(9, w - 1, 0, 0) ;
	mvwprintw(text, 1, 2, "Press one of the following keys to continue:") ;
	mvwprintw(text, 3, 4, "c -- create a zombie") ;
	mvwprintw(text, 4, 4, "d -- delete a created zombie") ;
	mvwprintw(text, 5, 4, "D -- delete all created zombies") ;
	mvwprintw(text, 6, 4, "q -- delete all created zombies and quit") ;
	box(text, 0, 0) ;
	wrefresh(text) ;

	WINDOW* report = newwin(3, w - 1, 9, 0) ;
	box(report, 0, 0) ;
	wrefresh(report) ;

	while ((ch = wgetch(report)) != 'q')
	{
		werase(report) ;
		switch(ch)
		{
			case 'c':
				mvwprintw(report, 1, 2, "Create a zombie") ;
				break ;
			case 'd':
				mvwprintw(report, 1, 2, "Delete a created zombie") ;
				break ;
			case 'D':
				mvwprintw(report, 1, 2, "Delete all created zombies") ;
				break ;
			case 'q':
				mvwprintw(report, 1, 2, "Delete all created zombies and quit") ;
				break ;
			default:
				mvwprintw(report, 1, 2, "Unrecognised key") ;
				break ;
		}
		box(report, 0, 0) ;
		wrefresh(report) ;
	}

	endwin() ;

	return EXIT_SUCCESS ;

	//	TO BE REWORKED.

	pid_t pid ;
	int   i ;

	for ( i = 0 ; i < 10 ; ++i )
	{
		pid = fork( ) ;

		if ( pid == 0 )
		{
			// i am the child process.  i exit immediately.

			exit( 0 ) ;
		}
	}

	sleep( 300 ) ;

	return 0 ;
}
