/**	@file
 *	
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include "options.h"

int handle_help_options(
	const char argc,
	const char*const*const argv,
	const MessageWriter short_writer,
	const MessageWriter long_writer
)
{

	MessageWriter _long_writer ;
	wchar_t* name ;
	extern int optind ;
	int option ;
	int count ;

	//	I return failure if the command-line arguments or short message writer
	//	are not meaningful pointers.

	if (argv == NULL || short_writer == NULL)
	{
		return -1 ;
	}

	//	If the long message writer is not a meaningful pointer then I use the
	//	short message writer in its place.

	_long_writer = long_writer == NULL ? short_writer : long_writer ;

	//	I try to extract from the command-line arguments the name by which the
	//	program was invoked.  I return failure if I fail.

	name = widen_string(filename_pointer(argv[0])) ;
	if (name == NULL)
	{
		return -1 ;
	}

	///	@note I must cast `const` away from `**argv` when I call `getopt()`,
	///	but do so on the understanding that `getopt()` does not modify
	///	`**argv`.

	optind = 1 ;
	while ((option = getopt(argc, (char*const*const) argv, ":hH")) != -1)
	{
		switch (option)
		{
			case 'h':
				count = short_writer(stdout, name) ;
				free(name) ;
				return count ;
			case 'H':
				count = _long_writer(stdout, name) ;
				free(name) ;
				return count ;
		}
	}

	free(name) ;
	return 0 ;

}
