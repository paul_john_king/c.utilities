#ifndef hplcXUalmtlgRTntJEBmTiTHSMwCNKi
#define hplcXUalmtlgRTntJEBmTiTHSMwCNKi

/**	@file
 *	
 *	Create zombie processes.

 *	Usage
 *	-----
	
 *	The command call

 *	    romero [-h|-H]
	
 *	creates 10 zombie process and sleeps for 5 minutes.
 *	
 *	*  `-h` -- Write a short help message to standard output and exit.
 *	
 *	*  `-H` -- Write a long help message to standard output and exit.

 *	Details
 *	-------

 *	`romero` creates 10 child processes and sleeps for 5 minutes.  Each child
 *	process exits immediately after creation, and sends a `SIGCHILD` signal to
 *	the parent process. However, the parent process does not invoke `wait()` in
 *	response to the signal, and the exiting child remains in the process table
 *	as a zombie process. If the parent process wakes and exits or is killed
 *	then each child process is orphaned and `init` adopts each orphan process.
 *	`init` invokes `wait()` at regular intervals, after which each orphan
 *	process dies.
 *	
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include <locale.h>
#include <ncurses.h>

#include "options.h"

#endif//hplcXUalmtlgRTntJEBmTiTHSMwCNKi
