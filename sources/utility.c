/**	@file
 *	
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include "utility.h"

bool system_error(void)
{

	if (errno != 0)
	{
		perror("System error") ;
		return true ;
	}
	else
	{
		return false ;
	}

}

char* filename_pointer(const char*const string)
{

	int offset ;

	if (string == NULL)
	{
		return NULL ;
	}

	offset = 0 ;
	for (int i = 0 ; string[i] != '\0' ; ++i)
	{
		if (string[i] == '/')
		{
			offset = i + 1 ;
		}
	}

	return (char*) string + offset ;

}

bool seed_random(void)
{

	unsigned int seed = 0 ;
	int status ;

	errno = 0 ;
	status = syscall(SYS_getrandom, &seed, sizeof(unsigned int), GRND_RANDOM);
	if (status == -1)
	{
		system_error() ;
		return false ;
	}

	srand(seed) ;

	return true ;

}

unsigned int get_random(const unsigned int number)
{

	const unsigned int floor = (RAND_MAX % number) ;

	unsigned int random ;

	//	I ignore all numbers returned by `rand()` that do not exceed `floor`.

	while((random = rand()) <= floor){} ;

	return random % number ;

}

long int get_unow(void)
{

	struct timeval now ;

	//	I try to find the current time.  If I fail, I return -1.

	errno = 0 ;
	if (gettimeofday(&now, NULL) == -1)
	{
		system_error() ;
		return -1 ;
	}

	return (now.tv_sec * 1000000) + now.tv_usec ;

}
