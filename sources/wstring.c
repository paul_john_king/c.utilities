/**	@file
 *	
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include "wstring.h"

wchar_t* widen_string(const char*const in_string)
{

	size_t length ;
	wchar_t* out_string ;

	//	If the multibyte-character string is `NULL` then I return an empty
	//	wide-character string.

	if (in_string == NULL)
	{
		return L"\0" ;
	}

	//	I try to calculate the number of non-terminating wide characters that
	//	the wide-character string should have in order to hold wide-character
	//	equivalents of the non-terminating multibyte characters in the
	//	multibyte-character string.  If I fail then I return `NULL`.

	length = mbstowcs(NULL, in_string, 0) ;
	if (length == (size_t) -1)
	{
		return NULL ;
	}

	//	I try to allocate memory to the wide-character string just sufficient
	//	to hold wide-character equivalents of the non-terminating multibyte
	//	characters in the multibyte-character string, plus one terminating null
	//	wide character.  If I fail then I return `NULL`.

	errno = 0 ;
	out_string = (wchar_t*) calloc(length + 1, sizeof(wchar_t)) ;
	if (out_string == NULL)
	{
		if (system_error()) {
			return NULL ;
		}
		else
		{
			return L"\0" ;
		}
	}

	//	I try to write the wide-character equivalent of the multibyte-character
	//	string to the wide-character string.  If I fail then I return `NULL`.

	length = mbstowcs(out_string, in_string, length) ;
	if (length == (size_t) -1)
	{
		return NULL ;
	}

	//	I return the wide-character string.

	return out_string ;

}

wchar_t* concatenate_string_array(const size_t in_count, const wchar_t*const in_strings[in_count])
{

	size_t in_lengths[in_count] ;
	size_t out_length ;
	wchar_t* out_string ;

	//	I record the number of non-terminating characters in each element of
	//	the array, and calculate the total number of non-terminating characters
	//	in all the elements of the array.  I treat each `NULL` element as if it
	//	were an empty wide-character string.

	out_length = 0 ;
	for (size_t i = 0 ; i < in_count ; ++i)
	{
		if (in_strings[i] != NULL)
		{
			in_lengths[i] = wcslen(in_strings[i]) ;
			out_length += in_lengths[i] ;
		}
		else
		{
			in_lengths[i] = 0 ;
		}
	}

	//	I try to allocate just sufficient memory to the concatenation to hold
	//	the non-terminating characters in all the elements of the array, plus
	//	one terminating null wide character.  If I fail then I return `NULL`.

	errno = 0 ;
	out_string = (wchar_t*) calloc(out_length + 1, sizeof(wchar_t)) ;
	if (out_string == NULL && system_error())
	{
		return NULL ;
	}

	//	I write to the concatenation the non-terminating characters in all the
	//	elements of the array, plus one terminating null wide character.

	for (size_t i = 0, k = 0 ; i < in_count ; ++i)
	{
		for (size_t j = 0 ; j < in_lengths[i] ; ++j, ++k)
		{
			out_string[k] = in_strings[i][j] ;
		}
	}
	out_string[out_length] = L'\0' ;

	//	I return the concatenation.

	return out_string ;

}

wchar_t* concatenate_string_iterator(const size_t in_count, const va_list in_iterator)
{

	const wchar_t* in_strings[in_count] ;
	wchar_t* out_string ;

	//	I create an array from the iterants of the iterator, call
	//	`concatenate_string_array()` with the array in order to create the
	//	concatenation, and return the concatenation.

	for (size_t i = 0 ; i < in_count ; ++i)
	{
		in_strings[i] = va_arg(in_iterator, wchar_t*) ;
	}
	out_string = concatenate_string_array(in_count, in_strings) ;

	return out_string ;

}

wchar_t* concatenate_strings(const size_t in_count, ...)
{

	va_list in_iterator ;
	wchar_t* out_string ;

	//	I create an iterator over the strings, call
	//	`concatenate_string_iterator()` with the iterator in order to create
	//	the concatenation, and return the concatenation.

	va_start(in_iterator, in_count) ;
	out_string = concatenate_string_iterator(in_count, in_iterator) ;
	va_end(in_iterator) ;

	return out_string ;

}

wchar_t* overwrite_string(const wchar_t character, const wchar_t*const in_string)
{

	size_t length ;
	wchar_t* out_string ;

	//	If the string is `NULL` then I return an empty wide-character string.

	if (in_string == NULL)
	{
		return L"\0" ;
	}

	//	I record the number of non-terminating characters in the input string.

	length = wcslen(in_string) ;

	//	I try to allocate just sufficient memory to the output string to hold
	//	as many non-terminating characters as the input string, plus one
	//	terminating null wide character.  If I fail then I return `NULL`.

	out_string = (wchar_t*) calloc(length + 1, sizeof(wchar_t)) ;
	if (out_string == NULL && system_error())
	{
		return NULL ;
	}

	//	I write to the ouput string one `-` wide character for each
	//	non-terminating character in the input string.

	for (size_t i = 0 ; i < length ; ++i)
	{
		switch (in_string[i])
		{
			case L'\n':
			case L'\t':
				out_string[i] = in_string[i] ;
			break ;
			default:
				out_string[i] = character ;
			break ;
		}
	}
	out_string[length] = L'\0' ;

	//	I return the output string.

	return out_string ;

}

wchar_t* overwrite_string_iterator(const wchar_t character, const size_t in_count, const va_list in_iterator)
{

	wchar_t* in_string ;
	wchar_t* out_string ;

	in_string = concatenate_string_iterator(in_count, in_iterator) ;
	if (in_string == NULL)
	{
		return NULL ;
	}

	out_string = overwrite_string(character, in_string) ;
	free(in_string) ;
	if (out_string == NULL)
	{
		return NULL ;
	}

	return out_string ;

}

wchar_t* overwrite_strings(const wchar_t character, const size_t in_count, ...)
{

	va_list in_iterator ;
	wchar_t* out ;

	va_start(in_iterator, in_count) ;
	out = overwrite_string_iterator(character, in_count, in_iterator) ;
	va_end(in_iterator) ;

	return out ;

}

wchar_t* underline_string_iterator(const wchar_t character, const size_t in_count, const va_list in_iterator)
{

	wchar_t* in_string ;
	size_t in_length ;
	wchar_t* out_string ;

	//	I try to concatenate the iterants and count the occurrences of
	//	characters in the concatenation.  If I fail, I return `NULL`.

	in_string = concatenate_string_iterator(in_count, in_iterator) ;
	if (in_string == NULL)
	{
		return NULL ;
	}
	in_length = wcslen(in_string) ;

	//	I try to allocate memory to the output string just sufficient to hold
	//	
	//	*   each occurrence of a character in the concatenation and its
	//	    underline character,
	//	
	//	*   a newline character after the last occurrence of a concatenation
	//	    character, and
	//	
	//	*   a null terminator.
	//	
	//	If I fail, I free allocated memory and return `NULL`.

	out_string = (wchar_t*) calloc((2 * in_length) + 2, sizeof(wchar_t)) ;
	if (out_string == NULL)
	{
		free(in_string) ;
		if (system_error()) {
			return NULL ;
		}
		else
		{
			return L"\0" ;
		}
	}

	//	I write concatenation and underline characters to the output string.  I
	//	set indices `i` and `j` to indicate the first occurrence of a character
	//	in the concatenation, and increment `i` to indicate each occurence of a
	//	character in the concatenation (including the null terminator) in turn.
	//	If `i` indicates a character other than a line ending, I append that
	//	character to the output string.  But if `i` indicates a line ending (a
	//	new-line character or the null terminator), I append a new-line
	//	character to the output string (even if `i` indicates the null
	//	terminator: the output string is unfinished because the latest
	//	characters appended to the output string are not yet underlined), and
	//	increment `j` to also indicate each occurrence of a character in the
	//	concatenation (including the null terminator) in turn until `j` catches
	//	up with `i`.  If `j` indicates an underlineable character, I append the
	//	underline character.  But if `j` indicates a non-underlineable
	//	character (a tab character, a new-line character or the null
	//	character), I append that character to the output string.

	//	For example, suppose that the concatenation is

	//		XX\tXX\nYYYY\0

	//	and the underline character is `-`.  I perform the stages

	//		concatenation    output string
	//		-------------    -------------
	//	
	//		i
	//		│
	//		XX\tXX\nYYYY\0 → \0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0
	//		│
	//		j
	//	
	//		⋮
	//	
	//		      i
	//		      │
	//		XX\tXX\nYYYY\0 → XX\tXX\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0
	//		│
	//		j
	//	
	//		⋮
	//	
	//		      i
	//		      │
	//		XX\tXX\nYYYY\0 → XX\tXX\n--\t--\n\0\0\0\0\0\0\0\0\0\0
	//		      │
	//		      j
	//	
	//		⋮
	//	
	//		            i
	//		            │
	//		XX\tXX\nYYYY\0 → XX\tXX\n--\t--\nYYYY\n\0\0\0\0\0
	//		      │
	//		      j
	//	
	//		⋮
	//	
	//		            i
	//		            │
	//		XX\tXX\nYYYY\0 → XX\tXX\n--\t--\nYYYY\n----\0
	//		            │
	//		            j


	for (size_t i = 0, j = 0 ; i <= in_length ; ++i)
	{
		switch (in_string[i])
		{
			case L'\0':
			case L'\n':
				out_string[i + j] = L'\n' ;
				for (; j <= i ; ++j)
				{
					switch (in_string[j])
					{
						case L'\0':
						case L'\n':
						case L'\t':
							out_string[i + 1 + j] = in_string[j] ;
							break ;
						default:
							out_string[i + 1 + j] = character ;
							break ;
					}
				}
				break ;
			default:
				out_string[i + j] = in_string[i] ;
				break ;
		}
	}

	//	I free allocated memory and return the output string.

	free(in_string) ;
	return out_string ;

}

wchar_t* underline_strings(const wchar_t character, const size_t in_count, ...)
{

	va_list in_iterator ;
	wchar_t* out_string ;

	//	I create an iterator over the strings, call
	//	`underline_string_iterator()` with the iterator in order to create the
	//	underlined concatenation, and return the underlined concatenation.

	va_start(in_iterator, in_count) ;
	out_string = underline_string_iterator(character, in_count, in_iterator) ;
	va_end(in_iterator) ;

	return out_string ;

}
