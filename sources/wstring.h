#ifndef QJFvYTUgrRTnbkxEdIPJeZbJtLxgUjD
#define QJFvYTUgrRTnbkxEdIPJeZbJtLxgUjD

/**	@file
 *	
 *	Create and manipulate wide-character strings.
 *	
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <wchar.h>

#include "utility.h"

/** Try to return a non-`NULL` wide-character equivalent of a
 * multibyte-character string.
 *	
 *	@param in_string -- `NULL` or a null-terminated multibyte-character string.
 *	
 *	@return If `in_string` is `NULL`, return an empty wide-character string.
 *	If `in_string` is not `NULL`, try to return a string in which each
 *	multibyte character of `in_string` (including the null terminator) is
 *	replaced by the equivalent wide character.  If an error occurs, return
 *	`NULL`.
 *	
 *	@warning If `in_string` is neither `NULL` nor null-terminated, the
 *	behaviour of this function is undefined.
 */

wchar_t* widen_string(const char*const in_string) ;

/**	Try to return a non-`NULL` concatenation of the elements of an array of
 * wide-character strings.
 *	
 *	@param in_strings -- An array of `NULL`s and null-terminated wide-character
 *	strings.
 *	
 *	@param in_count -- The number of elements in `in_strings`.
 *	
 *	@return Try to return a concatenation of the elements of `in_strings`,
 *	using an empty wide-character string in place of each `NULL`.  If an error
 *	occurs, return `NULL`.
 *	
 *	@warning If an element of `in_strings` is neither `NULL` nor
 *	null-terminated, the behaviour of this function is undefined.
 */

wchar_t* concatenate_string_array(const size_t in_count, const wchar_t*const in_strings[in_count]) ;

/**	Try to return a non-`NULL` concatenation of the iterants of an iterator
 * over wide-character strings.
 *	
 *	@param in_iterator -- An iterator over `NULL`s and null-terminated
 *	wide-character strings (objects of type `wchar_t*`).
 *	
 *	@param in_count -- The number of iterants in `in_iterator`.
 *	
 *	@return Try to return a concatenation of the iterants of `in_iterator`,
 *	using an empty wide-character string in place of each `NULL`.  If an error
 *	occurs, return `NULL`.
 *	
 *	@warning If an iterant of `in_iterant` is neither `NULL` nor
 *	null-terminated, the behaviour of this function is undefined.
 */

wchar_t* concatenate_string_iterator(const size_t in_count, const va_list in_iterator) ;

/**	Try to return a non-`NULL` concatenation of some wide-character strings.
 *	
 *	@param ... -- Some `NULL`s and null-terminated wide-character strings
 *	(objects of type `wchar_t*`).
 *	
 *	@param in_count -- The number of `NULL`s and null-terminated wide-character
 *	strings.
 *	
 *	@return Try to return a concatenation of the `NULL`s and wide-character
 *	strings, using an empty wide-character string in place of each `NULL`.  If
 *	an error occurs, return `NULL`.
 *	
 *	@warning If a variadic argument is neither `NULL` nor null-terminated, the
 *	behaviour of this function is undefined.
 */

wchar_t* concatenate_strings(const size_t in_count, ...) ;

/**	Try to return a non-`NULL` output string in which a given wide character
 * replaces each occurrence of a non-newline and non-tab character in a
 * wide-character input string.
 *	
 *	@param character -- A wide character.
 *	
 *	@param in_string -- `NULL` or a null-terminated wide-character string.
 *	
 *	@return If `in_string` is `NULL`, return an empty wide-character string.
 *	If `in_string` is not `NULL`, try to return a string in which `character`
 *	replaces each occurrence of a non-newline and non-tab character in
 *	`in_string`.  If an error occurs, return `NULL`.
 *	
 *	@warning If `in_string` is neither `NULL` nor null-terminated, the
 *	behaviour of this function is undefined.
 */

wchar_t* overwrite_string(const wchar_t character, const wchar_t*const in_string) ;

/**	Try to return a non-`NULL` output string in which a given wide character
 * replaces each occurrence of a non-newline and non-tab character in a
 * concatenation of the iterants of an iterator over wide-character strings.
 *	
 *	@param character -- A wide character.
 *	
 *	@param in_iterator -- An iterator over `NULL`s and null-terminated
 *	wide-character strings.
 *	
 *	@param in_count -- The number of iterants in `in_iterator`.
 *	
 *	@return Try to return a string in which `character` replaces each
 *	occurrence of a non-newline and non-tab character in a concatenation of the
 *	iterants of `in_string`, using an empty wide-character string in place of
 *	each `NULL`.  If an error occurs, return `NULL`.
 *	
 *	@warning If an iterant of `in_iterant` is neither `NULL` nor
 *	null-terminated, the behaviour of this function is undefined.
 */

wchar_t* overwrite_string_iterator(const wchar_t character, const size_t in_count, const va_list in_iterator) ;

/**	Try to return a non-`NULL` output string in which a given wide character
 * replaces each occurrence of a non-newline and non-tab character in a
 * concatenation of some wide-character strings.
 *	
 *	@param character -- A wide character.
 *	
 *	@param ... -- Some `NULL`s and null-terminated wide-character strings
 *	(objects of type `wchar_t*`).
 *	
 *	@param in_count -- The number of `NULL`s and null-terminated wide-character
 *	strings.
 *	
 *	@return Try to return a string in which `character` replaces each
 *	occurrence of a non-newline and non-tab character in a concatenation of the
 *	`NULL`s and wide-character strings, using an empty wide-character string in
 *	place of each `NULL`.  If an error occurs, return `NULL`.
 *	
 *	@warning If a variadic argument is neither `NULL` nor null-terminated, the
 *	behaviour of this function is undefined.
 */

wchar_t* overwrite_strings(const wchar_t character, const size_t in_count, ...) ;

/**	Try to return a non-`NULL` underlined concatenation of the iterants of an
 * iterator over wide-character strings.
 *	
 *	@param character -- A wide character.
 *	
 *	@param in_iterator -- An iterator over `NULL`s and null-terminated
 *	wide-character strings (objects of type `wchar_t*`).
 *	
 *	@param in_count -- The number of iterants in `in_iterator`.
 *	
 *	@return Try to return an underlined concatenation of the iterants of
 *	`in_iterator`, using an empty wide-character string in place of each
 *	`NULL`.  Each non-tab character in each line of the concatenation is
 *	underlined with `character`.  If an error occurs, return `NULL`.
 *	
 *	@note Leading or trailing new lines or white space are neither added to nor
 *	removed from the wide-character strings or their concatenation.
 *	
 *	@warning If an iterant of `in_iterant` is neither `NULL` nor
 *	null-terminated, the behaviour of this function is undefined.
 *	
 *	@see underline_strings.
 */

wchar_t* underline_string_iterator(const wchar_t character, const size_t in_count, const va_list in_iterator) ;

/**	Try to return a non-`NULL` underlined concatenation of some wide-character
 * strings.
 *	
 *	@param character -- A wide character.
 *	
 *	@param ... -- Some `NULL`s and null-terminated wide-character strings
 *	(objects of type `wchar_t*`).
 *	
 *	@param in_count -- The number of `NULL`s and null-terminated wide-character
 *	strings.
 *	
 *	@return Try to return an underlined concatenation of the `NULL`s and
 *	wide-character strings, using an empty wide-character string in place of
 *	each `NULL`.  Each non-tab character in each line of the concatenation is
 *	underlined with `character`.  If an error occurs, return `NULL`.
 *	
 *	@note Leading or trailing new lines or white space are neither added to nor
 *	removed from the wide-character strings or their concatenation.
 *	
 *	@warning If a variadic argument is neither `NULL` nor null-terminated, the
 *	behaviour of this function is undefined.
 *	
 *	The function underlines each non-tab character in each line of the
 *	concatenation of the strings.  For example, the code

 *	    setlocale(LC_ALL, "") ;
 *	    wchar_t* text = underline_strings(L'-', 1, L"this text is underlined") ;
 *	    wprintf(L"%ls\n", text) ;
 *	    free(text) ;

 *	would write

 *	    this text is underlined
 *	    -----------------------

 *	to standard output.  A tab-space character is not underlined, but instead
 *	introduces a tab space into the underlining.  For example, the code

 *	    setlocale(LC_ALL, "") ;
 *	    wchar_t* text = underline_strings(L'-', 1, L"this text contains a tab\tspace") ;
 *	    wprintf(L"%ls\n", text) ;
 *	    free(text) ;

 *	would write something like

 *	    this text contains a tab        space
 *	    ------------------------        -----

 *	to standard output, depending on the size of the tab space.  A new-line
 *	character splits the text into lines, with each line separately underlined.
 *	For example, the code

 *	    setlocale(LC_ALL, "");
 *	    wchar_t* text = underline_strings(L'-', 1, L"this text contains a new\nline") ;
 *	    wprintf(L"%ls\n", text) ;
 *	    free(text) ;

 *	would write

 *	    this text contains a new
 *	    ------------------------
 *	    line
 *	    ----

 *	The function does not add white-space characters between the
 *	components of the concatenation.  For example, the code

 *	    setlocale(LC_ALL, "");
 *	    wchar_t* text = underline_strings(L'-', 5, L"this", L"text", L"has", L"five", L"components") ;
 *	    wprintf(L"%ls\n", text) ;
 *	    free(text) ;

 *	would write

 *	    thistexthasfivecomponents
 *	    -------------------------

 *	to standard output.  Nor does it add or remove leading or trailing new-line
 *	characters to or from the concatenation.  For example, the code

 *	    setlocale(LC_ALL, "");
 *	    wchar_t* text = underline_strings(L'-', 1, L"text") ;
 *	    wprintf(L"%ls%ls%ls\n", L"<<<<", text, L">>>>") ;
 *	    free(text) ;

 *	would write

 *	    <<<<text
 *	    ---->>>>

 *	to standard output, while the code

 *	    setlocale(LC_ALL, "");
 *	    wchar_t* text = underline_strings(L'-', 1, L"\ntext\n") ;
 *	    wprintf(L"%ls%ls%ls\n", L"<<<<", text, L">>>>") ;
 *	    free(text) ;

 *	would write

 *	    <<<<
 *	    
 *	    text
 *	    ----
 *	    
 *	    >>>>

 *	to standard output.  Notice the *two* new-line characters between `<<<<`
 *	and `text`, and between `----` and `>>>>`.  The new-line characters in
 *	`\ntext\n` break it into three strings: a first empty string, the string
 *	`text`, and a second empty string.  The code therefore writes

 *	    <<<<    (1)
 *	            (2)
 *	    text    (3)
 *	    ----    (4)
 *	            (5)
 *	    >>>>    (6)

 *	to standard output, where
 *
 *	*   (1) is the string `<<<<` followed by the first empty string,
 *
 *	*   (2) is an empty string underlining the first empty string,
 *
 *	*   (3) is the string `text`,
 *
 *	*   (4) is a string `----` underlining the string `text`,
 *
 *	*   (5) is the second empty string, and
 *
 *	*	(6) is an empty string underlining the second empty string followed by
 *	    the string `>>>>`.
 */

wchar_t* underline_strings(const wchar_t character, const size_t in_count, ...) ;

#endif//QJFvYTUgrRTnbkxEdIPJeZbJtLxgUjD
