#ifndef aKgPaYTrmyzNHKkgYgcwKvCGiFovSkv
#define aKgPaYTrmyzNHKkgYgcwKvCGiFovSkv

/**	@file
 *	
 *	Handle command-line help options.
 *	
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include "wstring.h"

/** Functions that try to write to an output stream a message about a named
 * entity.
 *
 *	@param stream -- An output stream.
 *
 *	@param name -- A name.
 *
 *	@returns Try to write to `stream` a message about an entity using `name` to
 *	identify the entity, and return the number of characters written (excluding the
 *	terminating null character).  If an error occurs, return a negative number.
 */

typedef int(*MessageWriter)(FILE*const stream, const wchar_t*const name) ;

/**	Try to write to standard output a help message about a program if the
 * program's command-line arguments contain a help option "-h" or "-H".
 *
 *	@param argv -- An array of command-line arguments.
 *
 *	@param argc -- The number of command-line arguments.
 *
 *	@param short_writer -- A message-writer function that writes a short help
 *	message.
 *
 *	@param long_writer -- `NULL` or a message-writer function that writes a long
 *	help message.
 *
 *	@return If `argv` contains the short help option "-h", call `short_writer`
 *	with a program name extracted from `argv` in order to try to write to
 *	standard output a help message, and return the number of characters written
 *	(excluding the terminating null character).  Otherwise, if `argv` contains
 *	the long help option "-H", call `long_writer` (or `short_writer` if
 *	`long_writer` is `NULL`) with a program name extracted from `argv` in order
 *	to try to write to standard output a help message, and return the number of
 *	characters written (excluding the terminating null character).  Otherwise,
 *	do nothing.  If an error occurs, return a negative number.
 */

int handle_help_options(
	const char argc,
	const char*const*const argv,
	const MessageWriter short_writer,
	const MessageWriter long_writer
) ;

#endif//aKgPaYTrmyzNHKkgYgcwKvCGiFovSkv
