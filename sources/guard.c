/**	@file
 *	
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include "guard.h"

/**	Try to write to an output stream a short help message about the program.
 *	
 *	@param stream -- An output stream.
 *	
 *	@param name -- A name.
 *	
 *	@return Try to write to `stream` a short help message about the program
 *	using `name` to identify the program, and return the number of characters
 *	written (excluding the terminating null character).  If an error occurs,
 *	return -1.
 */

static int write_short_help_message(FILE*const stream, const wchar_t*const name)
{

	int count ;

	if (stream == NULL || name == NULL)
	{
		return -1 ;
	}

	count = fwprintf(
		stream, 
		LR"(The command call

    %1$ls [-h|-H]

tries to write to standard output a C-preprocessor include guard of the form

    #ifndef <identifier>
    #define <identifier>
    #endif//<identifier>

where `<identifier>` is a valid C identifier comprising 31 random characters.

*  `-h` -- Write a short help message to standard output and exit.

*  `-H` -- Write a long help message to standard output and exit.
)",
		name
	) ;

	if (count < 0)
	{
		return -1 ;
	}

	return count ;

}

/**	Try to write to an output stream a long help message about the program.
 *	
 *	@param stream -- An output stream.
 *	
 *	@param name -- A name.
 *	
 *	@return Try to write to `stream` a long help message about the program
 *	using `name` to identify the program, and return the number of characters
 *	written (excluding the terminating null character).  If an error occurs,
 *	return -1.
 */

static int write_long_help_message(FILE*const stream, const wchar_t*const name)
{

	int count ;
	int cumulative_count ;

	if (stream == NULL || name == NULL)
	{
		return -1 ;
	}

	cumulative_count = 0 ;

	count = fwprintf(
		stream, 
		LR"(Write an include guard to standard output.

Usage
-----

)"
	) ;
	if (count < 0)
	{
		return -1 ;
	}
	else
	{
		cumulative_count += count ;
	}

	count = write_short_help_message(stream, name) ;
	if (count < 0)
	{
		return -1 ;
	}
	else
	{
		cumulative_count += count ;
	}

	count = fwprintf(
		stream, 
		LR"(
Details
-------

An include guard (see https://en.wikipedia.org/wiki/Include_guard) prevents any
text between the `define` and `endif` directives from being processed more than
once, even if the file containing the directives is included more than once.

If two files guard text with include guards that use the same identifier, then
processing one of the files will prevent processing the guarded text of the
other.  In order to prevent this, different files should use different
identifiers in their include guards.  To that end, `%1$ls` uses a pseudo-random
number generator in order to try to ensure that `<identifier>` is extremely
likely to be unique each time the program is called.

Note: A valid C identifier may begin with the character `_`, but `<identifier>`
never does so that it never conflicts with a system name.

Note: A valid C identifier may have any positive number of characters, but
`<identifier>` comprises exactly 31 characters because a C compiler can only
distinguish the first 31 characters of an identifier.

Note: `%1$ls` blocks if the high-quality Linux entropy pool `/dev/random` has
too few random bits to seed the pseudo random number generator.  To unblock,
generate environmental noise within the system by, for example, typing on the
system keyboard or moving the system mouse.

Warning: Do **not** use `%1$ls` as a crytographic strength pseudo-random string
generator.  `%1$ls` tries to ensure that `<identifier>` is unique.  It does
**not** try to ensure that `<identifier>` is unpredictable.

Warning: `%1$ls` does **not** use a crytographic strength pseudo-random number
generator.  It tries to ensure that `<identifier>` is unique.  It does **not**
try to ensure that `<identifier>` is unpredictable.
)",
		name
	) ;

	if (count < 0)
	{
		return -1 ;
	}
	else
	{
		cumulative_count += count ;
	}

	return cumulative_count ;

}

/** @private */

int main(const int argc, const char*const*const argv)
{

	//	The base alphabet comprises the alphanumeric characters and the `_`
	//	character.  The init alphabet comprises the alphabetic characters.  I
	//	create a valid C identifier comprising a character from the init
	//	alphabet and up to 30 characters from the base alphabet.
	//	
	//	Note: A valid C identifier may begin with the `_` character, but I
	//	forbid this in order to ensure that I never create an identifier that
	//	conflicts with a system name.
	//	
	//	Note: A valid C identifier may have any positive number of characters,
	//	but I allow exactly 31 characters because a C compiler can only
	//	distinguish the first 31 characters of an identifier.

	const char* const base_alphabet = "0123456789_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ;
	const char* const init_alphabet = base_alphabet + 11 ;

	const size_t base_number = strlen(base_alphabet) ;
	const size_t init_number = strlen(init_alphabet) ;

	int status ;
	char* identifier ;

	setlocale(LC_ALL, "") ;

	//	If my command-line arguments contain a help option "-h" or "-H", I try
	//	to write to standard output a help message about myself and return
	//	success.  If an error occurs, I return failure.

	status = handle_help_options(argc, argv, write_short_help_message, write_long_help_message) ;
	if (status < 0)
	{
		return EXIT_FAILURE ;
	}
	if (status > 0)
	{
		return EXIT_SUCCESS ;
	}

	//	I try to allocate memory to the identifier just sufficient to hold 31
	//	characters and a null terminator.  If I fail, I return failure.

	errno = 0 ;
	identifier = calloc(32, sizeof(char)) ;
	if (identifier == NULL)
	{
		if (system_error()) {
			return EXIT_FAILURE ;
		}
	}

	//	I try to seed the pseudo random number generator.  If I fail, return
	//	failure.

	status = seed_random() ;
	if (!status)
	{
		return EXIT_FAILURE ;
	}

	//	I construct an identifier comprising 1 random character from the init
	//	alphabet and 30 random characters from the base alphabet.

	identifier[0] = init_alphabet[get_random(init_number)] ;

	for(int i = 1 ; i < 31 ; ++i)
	{
		identifier[i] = base_alphabet[get_random(base_number)] ;
	}

	identifier[31] = '\0' ;

	//	I write to standard output a C header-file guard that uses the
	//	identifier.

	printf("#ifndef %1$s\n#define %1$s\n#endif//%1$s\n", identifier) ;

	//	I free allocated memory and return success.

	free(identifier) ;
	return EXIT_SUCCESS ;

}
