#ifndef oXveWyEZLEGzvMMyEeBIOzReXewCNTu
#define oXveWyEZLEGzvMMyEeBIOzReXewCNTu

/**	@file
 *	
 *	Write an include guard to standard output.

 *	Usage
 *	-----

 *	The command call
 *	
 *	    guard [-h|-H]
 *	
 *	tries to write to standard output a C-preprocessor include guard of the form

 *	    #ifndef <identifier>
 *	    #define <identifier>
 *	    #endif//<identifier>

 *	where `<identifier>` is a valid C identifier comprising 31 random
 *	characters.
 *	
 *	*  `-h` -- Write a short help message to standard output and exit.
 *	
 *	*  `-H` -- Write a long help message to standard output and exit.

 *	Details
 *	-------

 *	An include guard (see https://en.wikipedia.org/wiki/Include_guard) prevents
 *	any text between the `define` and `endif` directives from being processed
 *	more than once, even if the file containing the directives is included more
 *	than once.
 *	
 *	If two files guard text with include guards that use the same identifier,
 *	then processing one of the files will prevent processing the guarded text
 *	of the other.  In order to prevent this, different files should use
 *	different identifiers in their include guards.  To that end, `guard` uses a
 *	pseudo-random number generator in order to try to ensure that
 *	`<identifier>` is extremely likely to be unique each time the program is
 *	called.
 *	
 *	@note A valid C identifier may begin with the character `_`, but
 *	`<identifier>` never does so that it never conflicts with a system name.
 *	
 *	@note A valid C identifier may have any positive number of characters, but
 *	`<identifier>` comprises exactly 31 characters because a C compiler can
 *	only distinguish the first 31 characters of an identifier.
 *	
 *	@note `guard` blocks if the high-quality Linux entropy pool `/dev/random`
 *	has too few random bits to seed the pseudo random number generator.  To
 *	unblock, generate environmental noise within the system by, for example,
 *	typing on the system keyboard or moving the system mouse.
 *	
 *	@warning Do **not** use `guard` as a crytographic strength pseudo-random
 *	string generator.  `guard` tries to ensure that `<identifier>` is unique.
 *	It does **not** try to ensure that `<identifier>` is unpredictable.
 *
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include <locale.h>
#include <string.h>

#include "options.h"

#endif//oXveWyEZLEGzvMMyEeBIOzReXewCNTu
