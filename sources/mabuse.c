#define _XOPEN_SOURCE 700

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

void allocate_memory(__attribute__ ((unused)) int sig)
{
	int _errno ;
	void* memory ;
	size_t size = 1024*1024*1024 ;

	_errno = errno ;

	printf("call malloc\n") ;
	memory = malloc(size) ;
	if (memory == NULL){
		perror("Cannot allocate memory") ;
		return ;
	}
	memset(memory, 0, size) ;

	errno = _errno ;
}

int main()
{
	int status ;
	struct sigaction action ;

	errno = 0 ;

	//	I install function `allocate_memory` as a handler for signal `SIGUSR1`.

	action.sa_handler = allocate_memory ;
	action.sa_flags = 0 ;
	status = sigfillset(&action.sa_mask) ;
	if (status != 0) {
		perror("Cannot install signal handler") ;
		return errno ;
	}
	status = sigaction(SIGUSR1, &action, NULL) ;
	if (status != 0) {
		perror("Cannot install signal handler") ;
		return errno ;
	}

	//	I write my process ID to standard output, then wait indefinitely for
	//	signals.

	printf("PID: %d\nWaiting for signals...\n", getpid()) ;
	while(1)
	{
		pause() ;
	}

	return 0 ;
}
