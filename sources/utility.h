#ifndef FMjMzvDAaFIOreaQMdLpeWPQQKUvdQt
#define FMjMzvDAaFIOreaQMdLpeWPQQKUvdQt

/**	@file
 *	
 *	Provide miscellaneous utilities.
 *
 *	@copyright ©2019 Paul John King (paul_john_king@web.de).  All rights
 *	reserved.
 *	
 *	@copyright This program is free software: you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License, version 3 as
 *	published by the Free Software Foundation.
 *	
 *	@copyright This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *	Public License for more details.
 *	
 *	@copyright You should have received a copy of the GNU General Public
 *	License along with this program.  If not, see
 *	<http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <linux/random.h>

#include <sys/syscall.h> 
#include <sys/time.h>

/** Indicate and report a system error.
 *	
 *	@return If `errno` indicates a system error, write a description of the
 *	system error to standard error and return `true`.  Otherwise, return
 *	`false`.
 *	
 *	@see `man 3 errno`.
 */

bool system_error(void) ;

/** Try to return a pointer to the file-name component of a multibyte-character
 * path string.
 *	
 *	@param string -- `NULL` or a null-terminated multibyte-character path
 *	string.
 *	
 *	@return If `string` is `NULL`, return `NULL`.  Otherwise, if `string`
 *	contains no `/` character, return a pointer to `string`.  Otherwise, return
 *	a pointer to the character or null-terminator following the final `/`
 *	character in `string`.
 *	
 *	@note Like the GNU version, but unlike the POSIX version, of `basename()`,
 *	consider trailing `/` characters part of the path string and allow the
 *	file-name component of a path string to be empty.  Thus, the file-name
 *	component of an empty path string or a path string with a trailing `/`
 *	character is the empty string.
 *	
 *	@note This function does not return a copy of the file-name component of
 *	the path string, but rather a pointer to the address within the path string
 *	at which the file-name component begins.  Therefore, do not modify or free
 *	the path string until the pointer returned by this function is no longer
 *	needed.
 *	
 *	@warning If `string` is neither `NULL` nor null-terminated, the behaviour
 *	of this function is undefined.
 *	
 *	@see `man 3 basename`.
 */

char* filename_pointer(const char*const string) ;

/** Seed the pseudo random number generator from the high-quality Linux entropy
 * pool `/dev/random`.
 *	
 *	@return If the pseudo random number generator is succesfully seeded, return
 *	`true`.  Otherwise, return `false`.
 *	
 *	@note A call to this function blocks if the entropy pool has too few random
 *	bits to seed the pseudo random number generator.  To unblock the call,
 *	generate environmental noise within the system by, for example, having the
 *	user type on the system keyboard or move the system mouse.
 *	
 *	@see `man 4 random`.
 */

bool seed_random(void) ;

/** Return a random natural number less than a given natural number.
 *	
 *	@param number -- a natural number.
 *	
 *	@return Return a random natural number less than `number`.
 *	
 *	@note This function is unbiased -- no number less than `number` is more
 *	likely to be returned than any other.
 *	
 *	@note Before calling this function for the first time, seed the pseudo
 *	random number generator by, for example, calling `seed_random()`.
 *	
 *	@warning This function only returns crytographic strength random numbers if
 *	the pseudo random number generator is properly seeded by, for example,
 *	calling `seed_random()`.
 *	
 *	Many sources suggest that the operation

 *		rand() % number

 *	returns a random natural number less than `number`.  However, according to
 *	`man 3 rand`,

 *	>	The `rand()` function returns a pseudo-random integer in the range
 *	>	0 to RAND_MAX inclusive

 *	Consequently, the operation

 *		rand() % number

 *	may not return natural numbers less than `number` with equal probability --
 *	it tends to return smaller numbers slightly more often that larger numbers.
 *	For example, suppose that `RAND_MAX` is 9.  `rand()` will return 0, 1, 2,
 *	3, 4, 5, 6, 7, 8 or 9 with equal probability.  Suppose also that `number`
 *	is 4.  Now,
 *	
 *	*	`0 % 4 = 0`,
 *	
 *	*	`1 % 4 = 1`,
 *	
 *	*	`2 % 4 = 2`,
 *	
 *	*	`3 % 4 = 3`,
 *	
 *	*	`4 % 4 = 0`,
 *	
 *	*	`5 % 4 = 1`,
 *	
 *	*	`6 % 4 = 2`,
 *	
 *	*	`7 % 4 = 3`,
 *	
 *	*	`8 % 4 = 0`, and
 *	
 *	*	`9 % 4 = 1`.
 *	
 *	Therefore, on average,
 *	
 *	*	30% of the calls of `rand() % 4` will return 0, and
 *	
 *	*	30% of the calls of `rand() % 4` will return 1,
 *	
 *	but only
 *	
 *	*	20% of the calls of `rand() % 4` will return 2, and
 *	
 *	*	20% of the calls of `rand() % 4` will return 3.
 *	
 *	This bias can be rectified by selecting a floor, and ignoring all returns
 *	of `rand()` that do not exceed this floor.  The lowest such floor for this
 *	example is given by

 *		floor = 9 % 4 = 1

 *	If all calls of `rand()` that return 0 or 1 are ignored then, on
 *	average,
 *	
 *	*	25% of the calls of `rand() % 4` will return 0,
 *	
 *	*	25% of the calls of `rand() % 4` will return 1,
 *	
 *	*	25% of the calls of `rand() % 4` will return 2, and
 *	
 *	*	25% of the calls of `rand() % 4` will return 3.
 *	
 *	More generally, the lowest floor is given by
 *	
 *		floor = RAND_MAX % number
 *	
 *	@see `man 3 rand`.
 */

unsigned int get_random(const unsigned int number) ;

/** Try to return the number of microseconds from the epoch until now.
 *	
 *	@return Try to return the number of microseconds from the epoch until now.
 *	If an error occurs, return -1.
 */

long int get_unow(void) ;

#endif//FMjMzvDAaFIOreaQMdLpeWPQQKUvdQt
