#define _XOPEN_SOURCE 700

#include <errno.h>
#include <ftw.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

//	I set the program name to a default value here, but the `main` function
//	later sets it to the invocation extracted from the command line.

char* name = "chown_sd" ;

void output_help(){

	printf("Usage:\n") ;
	printf("\n") ;
	printf("    %s [-R] [-v] «uid» «gid» «path_1» ... «path_n»\n", name) ;
	printf("\n") ;
	printf("        first tries to set the IDs of the user and group that own paths\n") ;
	printf("        `«path_1»`, ..., `«path_n»` (and their descendents, if the `-R` option\n") ;
	printf("        is used) to `«uid»` and `«gid»` respectively, then deletes `%s`.  The\n", name) ;
	printf("        paths must exist beforehand, and the user must have the ability to change\n") ;
	printf("        the user and group IDs.  The call is verbose, if the `-v` option is used,\n") ;
	printf("        and returns `0` on success, `1` on a command-line error, and `2` on\n") ;
	printf("        other errors.\n") ;
	printf("\n") ;
	printf("        The build requires a GCC compiler, because the source code contains a\n") ;
	printf("        GCC extension of C.\n") ;
	printf("\n") ;
	printf("    %s -h\n", name) ;
	printf("\n") ;
	printf("        displays this help message.\n") ;
	printf("\n") ;

}

void delete_and_exit(const char* executable_path, int exit_status){

	unlink(executable_path) ;
	exit(exit_status) ;

}

long get_id_from_string(const char* string){

	errno = 0 ;
	const long id = strtol(string, NULL, 10) ;
	if(errno != 0){
		return -1 ;
	}

	const int buffer_size = strlen(string) + 1 ;
	char buffer[buffer_size] ;
	int status = snprintf(buffer, buffer_size, "%ld", id) ;
	if(status < 0){
		return -1 ;
	}

	if(strcmp(string, buffer) == 0){
		return id ;
	}else{
		return -1 ;
	}

}

int main(int argc, char** argv){

	const char* ME = argv[0] ;
	int status ;
	int index ;

	//	I set the program name to the invocation extracted from the command
	//	line.

	name = argv[0] ;

	for(index = 0; argv[0][index] != '\0'; ++index)
	{
		if(argv[0][index] == '/')
		{
			name = argv[0] + index + 1 ;
		}
	}

	//	I try to parse the command-line options.

	bool recursive = false ;
	bool verbose = false ;
	extern int optind ;
	extern int optopt ;

	while(1){
		status = getopt(argc, argv, ":hRv") ;
		if(status == -1){
			break ;
		}
		switch(status){
			case 'h':
				output_help() ;
				exit(0) ;
			case 'R':
				recursive = true ;
				break ;
			case 'v':
				verbose = true ;
				break ;
			default:
				fprintf(stderr, "%s error: I found invalid command-line option `-%c`.\n", name, optopt) ;
				delete_and_exit(ME, 1) ;
				break ;
		}
	}

	//	I try to parse the user and group IDs from the command-line arguments.

	if(argc - optind < 2){
		fprintf(stderr, "%s error: I found too few command-line arguments.\n", name) ;
		delete_and_exit(ME, 1) ;
	}

	char* uid = argv[optind + 0] ;
	const long UID = get_id_from_string(uid) ;
	if(UID < 0){
		fprintf(stderr, "%s error: I could not convert the command-line argument `%s` to an ID.\n", name, uid) ;
		delete_and_exit(ME, 1) ;
	}

	char* gid = argv[optind + 1] ;
	const long GID = get_id_from_string(gid) ;
	if(GID < 0){
		fprintf(stderr, "%s error: I could not convert the command-line argument `%s` to an ID.\n", name, gid) ;
		delete_and_exit(ME, 1) ;
	}

	//	I try to parse and process the paths from the command-line arguments.

	//	NOTE: I use an internal function in order to define a handler for the
	//	`ntfw` call that knows the user ID without making the user ID global.
	//	Internal functions are a GCC extension of C.

	int change_owner(const char* PATH,
		__attribute__ ((unused)) const struct stat* STAT,
		__attribute__ ((unused)) int flag,
		__attribute__ ((unused)) struct FTW* buffer
	){
		if(verbose){
			fprintf(stdout, "%s info: I am trying to change the ID of the owner of the path `%s` to `%lu`.\n", name, PATH, UID) ;
		}
		errno = 0 ;
		int status = lchown(PATH, UID, GID) ;
		if(errno != 0){
			fprintf(stderr, "%s error: I encountered the error '%s' while trying to change the ID of the owner of the path `%s` to `%lu`.\n", name, strerror(errno), PATH, UID) ;
		}
		return status ;
	}

	for(index = optind + 2 ; index < argc ; ++index){
		char* path = argv[index] ;

		//	I try to change the ownership of the path (and its descendents, if
		//	requested).

		if(recursive){
			if(verbose){
				fprintf(stdout, "%s info: I am trying to change the ID of the owner of the path `%s` and its descendents to `%lu`.\n", name, path, UID) ;
			}
			status = nftw(path, &change_owner, 32, FTW_DEPTH) ;
			if(status != 0){
				fprintf(stderr, "%s error: I could not change the ID of the owner of the path `%s` and its descendents to `%lu`.\n", name, path, UID) ;
			}
		}else{
			status = change_owner(path, NULL, 0, NULL) ;
		}
		if(status != 0){
			delete_and_exit(ME, 2) ;
		}
	}

	delete_and_exit(ME, 0) ;

	return 0 ;

}
