CC ::= gcc
CFLAGS ::= -std=gnu11 -Werror -Wall -Wextra -pipe
CFLAGS_TEST ::= -Og -g
CFLAGS_PROD ::= -O4 -s -static

VG ::= valgrind
VFLAGS ::= --quiet \
	--vgdb=full \
	--error-exitcode=1 \
	--sigill-diagnostics=yes
MEMCHECK ::= $(VG) $(VFLAGS) \
	--tool=memcheck \
	--leak-check=full \
	--errors-for-leak-kinds=all \
	--track-origins=yes

SOURCE_DIR ::= sources/
SOURCE_FILES ::= $(wildcard $(addprefix $(SOURCE_DIR),*.c))
HEADER_FILES ::= $(wildcard $(addprefix $(SOURCE_DIR),*.h))

OBJECT_DIR ::= objects/
OBJECT_TEST_DIR ::= $(OBJECT_DIR)test/
OBJECT_PROD_DIR ::= $(OBJECT_DIR)prod/
OBJECT_NAMES ::= $(addsuffix .o,$(basename $(notdir $(SOURCE_FILES))))
OBJECT_TEST_FILES ::= $(addprefix $(OBJECT_TEST_DIR),$(OBJECT_NAMES))
OBJECT_PROD_FILES ::= $(addprefix $(OBJECT_PROD_DIR),$(OBJECT_NAMES))

$(OBJECT_TEST_DIR)%: CFLAGS += $(CFLAGS_TEST)
$(OBJECT_PROD_DIR)%: CFLAGS += $(CFLAGS_PROD)

TARGET_DIR ::= targets/
TARGET_TEST_DIR ::= $(TARGET_DIR)test/
TARGET_PROD_DIR ::= $(TARGET_DIR)prod/
TARGET_NAMES ::= chown_sd guard mabuse romero
TARGET_TEST_FILES ::= $(addprefix $(TARGET_TEST_DIR),$(TARGET_NAMES))
TARGET_PROD_FILES ::= $(addprefix $(TARGET_PROD_DIR),$(TARGET_NAMES))

$(TARGET_TEST_DIR)%: CFLAGS += $(CFLAGS_TEST)
$(TARGET_PROD_DIR)%: CFLAGS += $(CFLAGS_PROD)

DOCUMENT_DIR ::= documents/

DOXYFILE ::= Doxyfile
CTAGS_FILE ::= $(addprefix $(SOURCE_DIR),tags)
ETAGS_FILE ::= $(addprefix $(SOURCE_DIR),TAGS)

define build_object
	@echo "Build '$(1)'."
	mkdir -p $(dir $(1))
	$(CC) $(CFLAGS) -c -o $(1) $(2)
	@echo
endef

define build_target
	@echo "Build '$(1)'."
	mkdir -p $(dir $(1))
	$(CC) $(CFLAGS) -o $(1) $(2)
	@echo
endef

define test_guard
	@echo "Test '$(1)'."
	$(MEMCHECK) $(1) -h > /dev/null
	$(MEMCHECK) $(1) -H > /dev/null
	$(MEMCHECK) $(1) > /dev/null
	@echo
endef

define test_romero
	@echo "Test '$(1)'."
	$(MEMCHECK) $(1) -h > /dev/null
	$(MEMCHECK) $(1) -H > /dev/null
	@echo
endef

define build_documents
	@echo "Build documents."
	DOCUMENT_DIR="$(1)" doxygen 1> /dev/null
	@echo
endef

define build_tags
	@echo "Build $(1)."
	$(1) -o $(2) $(3)
	@echo
endef

define delete_directory
	@echo "Delete '$(1)'."
	rm -f -r $(1)
	@echo
endef

define delete_existing_directory
	$(if $(wildcard $(1)),$(call delete_directory,$(1)))
endef

.PHONY: all targets objects documents tags clean

all: targets documents tags

targets: objects $(TARGET_TEST_FILES) $(TARGET_PROD_FILES)

objects: $(OBJECT_TEST_FILES) $(OBJECT_PROD_FILES)

documents: $(DOCUMENT_DIR)

tags: $(CTAGS_FILE) $(ETAGS_FILE)

clean:
	$(call delete_existing_directory,$(OBJECT_DIR))
	$(call delete_existing_directory,$(TARGET_DIR))
	$(call delete_existing_directory,$(DOCUMENT_DIR))

vpath %.c $(SOURCE_DIR)
vpath %.h $(SOURCE_DIR)

%/chown_sd.o: chown_sd.c
	$(call build_object,$(@),$(<))

%/guard.o: guard.c guard.h options.h
	$(call build_object,$(@),$(<))

%/mabuse.o: mabuse.c
	$(call build_object,$(@),$(<))

%/options.o: options.c options.h wstring.h
	$(call build_object,$(@),$(<))

%/romero.o: romero.c romero.h options.h
	$(call build_object,$(@),$(<))

%/utility.o: utility.c utility.h
	$(call build_object,$(@),$(<))

%/wstring.o: wstring.c wstring.h utility.h
	$(call build_object,$(@),$(<))

$(TARGET_DIR)%/chown_sd: $(OBJECT_DIR)%/chown_sd.o
	$(call build_target,$(@),$(^))

$(TARGET_DIR)%/guard: $(OBJECT_DIR)%/guard.o $(OBJECT_DIR)%/options.o $(OBJECT_DIR)%/utility.o $(OBJECT_DIR)%/wstring.o
	$(call build_target,$(@),$(^))
	$(if $(findstring test,$(*)),$(call test_guard,$(@)))

$(TARGET_DIR)%/mabuse: $(OBJECT_DIR)%/mabuse.o
	$(call build_target,$(@),$(^))

$(TARGET_DIR)%/romero: $(OBJECT_DIR)%/romero.o $(OBJECT_DIR)%/options.o $(OBJECT_DIR)%/utility.o $(OBJECT_DIR)%/wstring.o
	$(call build_target,$(@),$(^) -lncursesw -ltinfo)
	$(if $(findstring test,$(*)),$(call test_romero,$(@)))

$(DOCUMENT_DIR): $(SOURCE_FILES) $(HEADER_FILES) $(DOXYFILE)
	$(call build_documents,$(@))

$(CTAGS_FILE) : $(SOURCE_FILES) $(HEADER_FILES)
	$(call build_tags,ctags,$(@),$(^))

$(ETAGS_FILE) : $(SOURCE_FILES) $(HEADER_FILES) ;
	$(call build_tags,etags,$(@),$(^))
